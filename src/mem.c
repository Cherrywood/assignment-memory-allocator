#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    size_t region_size = region_actual_size(query);
     //пытаемся зарезервировать память в плотную
    void* map_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE); 
    if( map_addr == MAP_FAILED) {
        //если не получилось, позволяем mmap выбрать подходящий адрес для начала нового региона
        map_addr = map_pages(NULL, region_size, 0);
        if (map_addr == MAP_FAILED)
            return REGION_INVALID;
    }
    block_init(map_addr, (block_size){region_size},NULL);
    return (struct region) {
            .addr = map_addr,
            .size = region_size,
	    .extends = true
    };
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if(!block_splittable(block,query)) return false;
    //Разделяем блок на два блока: блок размера query и оставшаяся часть
    //инициализируем 2 блок
    void* second_block_addr = block->contents + query;
    block_init(second_block_addr, (block_size){block->capacity.bytes - query}, block->next);

    //У первого блока уменьшаем размер, а также даем ссылку на 2 блок
    block->capacity = (block_capacity) {query};
    block->next = second_block_addr;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if (block != NULL && block->next != NULL && mergeable(block, block->next)) {
            block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
            block->next = block->next->next;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    struct block_header* last_block =  block;
    struct block_header* current_block = block;
    while (current_block) {
        //прежде, чем решить, хороший ли блок - надо объединить его
        // со всеми идущими за ним свободными блоками
        while(try_merge_with_next(current_block));
        if(current_block->is_free && block_is_big_enough(sz,current_block)) {
            return (struct  block_search_result) {
                .type = BSR_FOUND_GOOD_BLOCK,
                .block = current_block
            };
        }
	last_block = current_block;
	current_block = last_block->next;
    }
    return (struct  block_search_result) {
            .type = BSR_REACHED_END_NOT_FOUND,
            .block = last_block
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result try_block = find_good_or_last(block,query);
    if (try_block.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(try_block.block, query);
	try_block.block->is_free = false;
    }
    return try_block;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    struct region new_region = alloc_region(block_after(last), query );
    if(region_is_invalid(&new_region))
        return NULL;
    last->next = new_region.addr;
    return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    query = size_max(query,BLOCK_MIN_CAPACITY);
    struct block_search_result mem_block = try_memalloc_existing(query, heap_start);
	//Если не нашли, то расширяем кучу и пытаемся снова    
    if (mem_block.type == BSR_REACHED_END_NOT_FOUND) {
    	struct block_header* heap = grow_heap(mem_block.block,query);
	if(heap== NULL) return NULL;
	mem_block = try_memalloc_existing(query,heap);
    }
    return mem_block.block;
}

void* _malloc( size_t query ) {
  
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  //при освобождении блока можно объединить его со всеми следующими за ним свободными блоками.
    while (try_merge_with_next(header));

}
