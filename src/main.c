#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>
#include "util.h"

void debug(const char* fmt, ... );
void init_heap();
void test_1();
void test_2();
void test_3();
void test_4();
void test_5();

int main() {
	 debug("Goooooo......\n");
	void* heap = heap_init(10000);
	if(heap == NULL) {
		err("Куча не инициализировалась");
	} 
	debug("Куча инициализировалась\n");
	debug_heap(stdout, heap);

 	test_1(heap);
	test_2(heap);
	test_3(heap);
	test_4(heap);
        test_5(heap);
}
