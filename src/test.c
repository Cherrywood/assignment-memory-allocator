#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>
#include "util.h"

 
void debug(const char* fmt, ... );
static struct block_header* get_header_block(void *mem){
	return (struct block_header*)((uint8_t*) mem - offsetof(struct block_header, contents));
}


//Как эти ваши тесты писать
	
void test_1(void const* heap) {
    debug("Тест 1. Обычное успешное выделение памяти\n");
    void* mem = _malloc(256);
    if(mem == NULL) {
        err("Ошибка. Память не выделилась\n");
    }

    struct block_header* block_header = get_header_block(mem);
    if (block_header->capacity.bytes != 256) {
	err("Ошибка. Размер не соответствует заданному\n");
    }
    if (block_header->is_free) {
	err("Ошибка. Блок не занят, хотя мы как бы его выделили. Что-то не так\n");
    }
    debug("Выделение памяти\n");
    debug_heap(stdout, heap);

    _free(mem);
    debug("Освобождение памяти\n");
    debug_heap(stdout, heap);
    debug("Тест 1. Конец\n");
}

void test_2(void const* heap) {
    debug("Тест 2. Освобождение одного блока из нескольких выделенных\n");
    void* mem_1 = _malloc(666);
    void* mem_2 = _malloc(1024);
    void* mem_3= _malloc(1024);
    if(mem_1 == NULL || mem_2 == NULL || mem_3 == NULL ) {
        err("Ошибка. Память где-то не выделилась\n");
    }
    debug("Выделение памяти\n");
    debug_heap(stdout, heap);

    _free(mem_2);
    debug("Освобождаем 2 участок памяти\n");
    debug_heap(stdout, heap);
        
    if (get_header_block(mem_1)->is_free) {
    err("Ошибка. Освобожден не тот блок\n");
    }
    if (!get_header_block(mem_2)->is_free) {
    err("Ошибка. Нужный блок не освобожден\n");
    }
    if (get_header_block(mem_3)->is_free) {
    err("Ошибка. Освобожден не тот блок\n");
    }
    _free(mem_1);
    _free(mem_3);
    debug("Тест 2. Конец\n");
}

void test_3(void const* heap) {
    debug("Тест 3. Освобождение двуз блоков из нескольких выделенных\n");
    void* mem_1 = _malloc(666);
    void* mem_2 = _malloc(1024);
    void* mem_3= _malloc(1024);
    void* mem_4= _malloc(70);
    if(mem_1 == NULL || mem_2 == NULL || mem_3 == NULL || mem_4 == NULL) {
        err("Ошибка. Память где-то не выделилась\n");
    }
    debug("Выделение памяти\n");
    debug_heap(stdout, heap);

    _free(mem_2);
    _free(mem_4);
    debug("Освобождаем 2 и 4 участки памяти\n");
    debug_heap(stdout, heap);

    if (get_header_block(mem_1)->is_free) {
    err("Ошибка. Освобожден не тот блок\n");
    }
    if (!get_header_block(mem_2)->is_free) {
    err("Ошибка. Нужный блок не освобожден\n");
    }
    if (get_header_block(mem_3)->is_free) {
    err("Ошибка. Освобожден не тот блок\n");
    }
    if (!get_header_block(mem_4)->is_free) {
    err("Ошибка. Нужный блок не освобожден\n");
    }

    _free(mem_1);
    _free(mem_3);
    debug("Тест 3. Конец\n");
}

void test_4(void const* heap) {
    debug("Тест 4. Память закончилась, новый регион памяти расширяет старый\n");
    void* mem_1 = _malloc(1024);
    void* mem_2= _malloc(1024);
    void* mem_3= _malloc(10000);

    debug("Выделение памяти\n");
    debug_heap(stdout, heap);
     if(mem_1 == NULL || mem_2 == NULL || mem_3 == NULL) {
        err("Ошибка. Память где-то не выделилась\n");
    }
    debug("Тест 4. Конец\n");
}

void test_5(void const* heap) {
    debug("Тест 5. Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n");
    void* mem_1= _malloc(9000);
    
    debug("Выделение памяти\n");
    debug_heap(stdout, heap);
    if(mem_1 == NULL) {
        err("Ошибка. Память не выделилась\n");
    }
   
    struct block_header* block_header_1 = get_header_block(mem_1);
    while(block_header_1->next !=NULL) {
	block_header_1 = block_header_1->next;
    }
    
    void* map_addr = mmap((void*)(block_header_1->contents + block_header_1->capacity.bytes), 10000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    if( map_addr == NULL ||  map_addr == MAP_FAILED) {
        err("Ошибка. Память не выделилась\n");
    } 
    
    void* mem_2 = _malloc(1024);
    debug("Выделение памяти\n");
    debug_heap(stdout, heap);

    struct block_header* block_header_2 = get_header_block(mem_2);
    if((void*)(block_header_1->contents + block_header_1->capacity.bytes) == (void*)block_header_2) {
	err("Ошибка. Память выделилась вплотную\n");
    }
    
    _free(mem_1);
    _free(mem_2);
    debug("Тест 5. Конец\n");
}
